<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Diagnostic System Base On Culture Method</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no">
    <meta name="msapplication-tap-highlight" content="no">
    <style type="text/css">
        <?php include 'View/assets/css/main.d810cf0ae7f39f28f336.css'; 
        include 'View/assets/css/style.css';
        ?>
    </style>
</head>

<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-header fixed-sidebar closed-sidebar-mobile closed-sidebar">
        <div class="app-header header-shadow">
            <div class="app-header__logo">
                <div>
                    <h5><b>TTQQ</b></h5>
                </div>
                <div class="header__pane ml-auto">
                    <div>
                        <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                            <span class="hamburger-box">
                                <span class="hamburger-inner">
                                </span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="app-header__mobile-menu">
                <div>
                    <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                        <span class="hamburger-box">
                            <span class="hamburger-inner">
                            </span>
                        </span>
                    </button>
                </div>
            </div>
            <div class="app-header__menu">
                <span>
                    <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                        <span class="btn-icon-wrapper">
                            <i class="fa fa-ellipsis-v fa-w-6">
                            </i>
                        </span>
                    </button>
                </span>
            </div>    
            <div class="app-header__content">
                <div class="app-header-left">
                    <div class="search-wrapper">
                        <div class="input-holder">
                            <input type="text" class="search-input" placeholder="Type to search">
                            <button class="search-icon">
                                <span>
                                </span>
                            </button>
                        </div>
                        <button class="close">
                        </button>
                    </div>
                </div>
                <div class="app-header-right">

                    <div class="header-btn-lg pr-0">
                        <div class="widget-content p-0">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left">
                                    <div class="btn-group">
                                        <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="p-0 btn">
                                            <img width="42" height="42" style="object-fit:cover" class="rounded-circle" src="https://vandieuhay.org/wp-content/uploads/2020/07/trung-y-112.jpg" alt>
                                            <i class="fa fa-angle-down ml-2 opacity-8">
                                            </i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>     
                </div>
            </div>
        </div>        

        <div class="app-main">
            <div class="app-sidebar sidebar-shadow">
                <div class="app-header__logo">
                    <div class="logo-src">
                    </div>
                    <div class="header__pane ml-auto">
                        <div>
                            <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner">
                                    </span>
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="app-header__mobile-menu">
                    <div>
                        <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                            <span class="hamburger-box">
                                <span class="hamburger-inner">
                                </span>
                            </span>
                        </button>
                    </div>
                </div>
                <div class="app-header__menu">
                    <span>
                        <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                            <span class="btn-icon-wrapper">
                                <i class="fa fa-ellipsis-v fa-w-6">
                                </i>
                            </span>
                        </button>
                    </span>
                </div>    
                <div class="scrollbar-sidebar ps ps--active-y">
                    <div class="app-sidebar__inner">
                        <ul class="vertical-nav-menu metismenu">
                            <li class="app-sidebar__heading">Menu</li>
                            <li>
                                <a href="/">
                                    <i class="metismenu-icon pe-7s-graph1">
                                    </i>Diagnostic
                                </a>
                            </li>
                            <li>
                                <a href="?mod=home&act=aboutus">
                                    <i class="metismenu-icon pe-7s-graph1">
                                    </i>About us
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="app-main__outer">
                <div class="container" style="margin-top : 20px">
                    <div class="card">
                        <div class="card-header">
                            <h4>Group 08</h4>
                        </div>
                        <div class="content" style="padding-left : 20px">
                            <p>Nguyễn Thị Thu Trang</p>
                            <p>Phạm Thị Quỳnh</p>
                            <p>Đỗ Thị Anh Thư</p>
                            <p>Lê Văn Quang</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="app-drawer-overlay d-none animated fadeIn">
    </div>
    <script type="text/javascript">
        <?php include 'View/assets/js/main.d810cf0ae7f39f28f336.js'; ?>
    </script>
</body>
</html>