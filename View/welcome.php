<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Diagnostic System Base On Culture Method</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <!-- Disable tap highlight on IE -->
    <meta name="msapplication-tap-highlight" content="no">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">
    <style type="text/css">
        <?php include 'View/assets/css/main.d810cf0ae7f39f28f336.css'; 
        include 'View/assets/css/style.css';
        ?>
    </style>
    <style type="text/css">
        #form_submit {
            margin-top : 30px;
        }
        .content-result {
            color : red;
            margin-top : 30px;
        }
        .error {
            display : none;
            color : red;
            font-size : 13px;
            margin-left : 5px;
        }
        .card {
            margin-top: 20px;
        }
        .content-card {
            padding-left : 10px;
        }
        .row {
            width : 100%;
        }
    </style>
</head>

<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-header fixed-sidebar closed-sidebar-mobile closed-sidebar">
        <div class="app-header header-shadow">
            <div class="app-header__logo">
                <div class="">
                 <h5><b>TTQQ</b></h5>
             </div>
             <div class="header__pane ml-auto">
                <div>
                    <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                        <span class="hamburger-box">
                            <span class="hamburger-inner">
                            </span>
                        </span>
                    </button>
                </div>
            </div>
        </div>
        <div class="app-header__mobile-menu">
            <div>
                <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                    <span class="hamburger-box">
                        <span class="hamburger-inner">
                        </span>
                    </span>
                </button>
            </div>
        </div>
        <div class="app-header__menu">
            <span>
                <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                    <span class="btn-icon-wrapper">
                        <i class="fa fa-ellipsis-v fa-w-6">
                        </i>
                    </span>
                </button>
            </span>
        </div>    
        <div class="app-header__content">
            <div class="app-header-left">
                <div class="search-wrapper">
                    <div class="input-holder">
                        <input type="text" class="search-input" placeholder="Type to search">
                        <button class="search-icon">
                            <span>
                            </span>
                        </button>
                    </div>
                    <button class="close">
                    </button>
                </div>
            </div>
            <div class="app-header-right">

                <div class="header-btn-lg pr-0">
                    <div class="widget-content p-0">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="btn-group">
                                    <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="p-0 btn">
                                        <img width="42" height="42" style="object-fit:cover" class="rounded-circle" src="https://vandieuhay.org/wp-content/uploads/2020/07/trung-y-112.jpg" alt>
                                        <i class="fa fa-angle-down ml-2 opacity-8">
                                        </i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>     
            </div>
        </div>
    </div>        

    <div class="app-main">
        <div class="app-sidebar sidebar-shadow">
            <div class="app-header__logo">
                <div class="logo-src">
                </div>
                <div class="header__pane ml-auto">
                    <div>
                        <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                            <span class="hamburger-box">
                                <span class="hamburger-inner">
                                </span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="app-header__mobile-menu">
                <div>
                    <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                        <span class="hamburger-box">
                            <span class="hamburger-inner">
                            </span>
                        </span>
                    </button>
                </div>
            </div>
            <div class="app-header__menu">
                <span>
                    <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                        <span class="btn-icon-wrapper">
                            <i class="fa fa-ellipsis-v fa-w-6">
                            </i>
                        </span>
                    </button>
                </span>
            </div>    
            <div class="scrollbar-sidebar ps ps--active-y">
                <div class="app-sidebar__inner">
                    <ul class="vertical-nav-menu metismenu">
                        <li class="app-sidebar__heading">Menu</li>
                        <li>
                            <a href="/">
                                <i class="metismenu-icon pe-7s-graph1">
                                </i>Diagnostic
                            </a>
                        </li>
                        <li>
                            <a href="?mod=home&act=aboutus">
                                <i class="metismenu-icon pe-7s-graph1">
                                </i>About us
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="app-main__outer">
            <div class="app-main__inner">
                <div class="app-page-title">
                    <div class="page-title-wrapper">
                        <div class="row">
                            <div class="col-md-8">
                                <legend>Diagnostic</legend>
                                <select class="js-example-basic-multiple form-control" name="states[]" multiple="multiple">
                                    <?php if(isset($pulse)) { foreach($pulse as $value) {?>
                                        <option value="<?=$value['id']?>"><?=$value['name']?></option>
                                    <?php }}?>
                                </select>
                                <form class="group-input" id="form_submit">

                                </form>
                                <div class="content-result">

                                </div>
                            </div>
                            <div class="col-md-4">
                                <form action="#" method="POST" role="form" id="search">
                                    <legend>Search Patient's Information </legend>
                                    <div class="form-group">
                                        <label for="">Identify number</label>
                                        <input type="number" class="form-control" id="search-input" placeholder="Identify number" name="cmnd">
                                    </div>      

                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </form>
                                <div class="information-patient">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="app-drawer-overlay d-none animated fadeIn">
</div>
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                <h4 class="modal-title">Add New Patient</h4>
            </div>
            <div class="modal-body">
                <form action="#" id="Addnew">
                    <input type="hidden" name="id" id="id">
                    <div class="form-group">
                        <label for="">Name</label>
                        <input type="text" class="form-control" id="name" placeholder="Name" name='name' required>
                    </div>
                    <div class="form-group">
                        <label for="">Phone Number</label>
                        <input type="text" class="form-control" id="phone" placeholder="Phone Number" name='phone' required>
                    </div>
                    <div class="form-group">
                        <label for="">Identify Number</label>
                        <input type="text" class="form-control" id="cmnd" placeholder="Identify Number" name='cmnd' required>
                    </div>
                    <div class="form-group">
                        <label for="">Address</label>
                        <input type="text" class="form-control" id="address" placeholder="Address" name='address' required>
                    </div>

                    <button type="submit" class="btn btn-primary float-right" id="form-add-edit-submit"></button>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    <?php include 'View/assets/js/main.d810cf0ae7f39f28f336.js'; ?>
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.3/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js">
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.7.6/handlebars.min.js"></script>
<script id="handlebars-tag" type="text/x-handlebars-template">
    {{#each pulses}}
    <div class="form-group">
        <label for="">{{name}}</label>
        <input type="text" name="{{id}}" class="form-control m-required" placeholder="Please enter weight">
        <p class="error"></p>
    </div>
    {{/each}}
    <div class="form-button">
        <button type="reset" class="btn btn-secondary">Reset</button>
        <button type="submit" class="btn btn-success">Diagnostic</button>
    </div>
</script>
<script id="handlebars-result" type="text/x-handlebars-template">
    <label>Result : </label>
    <span class="result">{{ symptom }}</span>
    {{#if weight}}<p>Probability : {{weight}} %</p>{{/if}}
</script>
<script id="handlebars-information-patient" type="text/x-handlebars-template">
    <div class="card">
        <div class="card-header">
            Patient Information
        </div>
        <div class="content-card">
            <div class="form-group">
                <label for=""><b>Name</b></label>
                <p>{{ data.name }}</p>
            </div>
            <div class="form-group">
                <label for=""><b>Address</b></label>
                <p>{{ data.address }}</p>
            </div>
            <div class="form-group">
                <label for=""><b>Phone Number</b></label>
                <p>{{ data.phone }}</p>
            </div>
            <div class="form-group">
                <label for=""><b>Identify Number</b></label>
                <p>{{ data.cmnd }}</p>
            </div>
        </div>
    </div>
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('.js-example-basic-multiple').select2();
        $('.js-example-basic-multiple').on('change', function() {
            var pulses = [];
            $(this).find('option:selected').each(function() {
                pulses.push({'id' : $(this).val(), 'name' : $(this).text()});
            });
            var template = $('#handlebars-tag').html();
            var templateScript = Handlebars.compile(template);
            var context = {
                'pulses' : pulses,
            };
            var html = templateScript(context);
            $('#form_submit').html(html);
        })
        $('#search').on('submit', function(e) {
            e.preventDefault();
            let input = $('#search-input').val();
            if (input == '') {
                toastr.error('Identify must be required');
                return;
            }
            var data = new FormData(this);
            $.ajax({
                url: '?mod=home&act=search',
                method: 'post',
                dataType:'JSON',
                processData: false,
                contentType: false,
                data:data,
                success:function(response){
                    console.log("response", response)
                    $("#myModal").modal('show');
                    if(response == '') {
                        $('.modal-title').html("Add New Patient");
                        $("#form-add-edit-submit").text("Add New");
                        $("#form").val("add");
                        $('#name').val('');
                        $('#phone').val('');
                        $('#cmnd').val('');
                        $('#address').val('');
                        $("#id").val('');
                        $('#cmnd').attr('readonly', false);
                    } else {
                        $('.modal-title').html("Patient Information");
                        $("#form-add-edit-submit").text("Diagnostic");
                        $('#name').val(response.name);
                        $('#phone').val(response.phone);
                        $('#cmnd').val(response.cmnd);
                        $('#address').val(response.address);
                        $("#id").val(response.id);
                        $('#cmnd').attr('readonly', true);
                    }
                }
            });
        })

        $('#Addnew').on('submit', function(e) {
            e.preventDefault();
            var data = new FormData(this);
            $.ajax({
                url: '?mod=home&act=add',
                method: 'post',
                dataType:'JSON',
                processData: false,
                contentType: false,
                data:data,
                success:function(response){
                    var template = $('#handlebars-information-patient').html();
                    var templateScript = Handlebars.compile(template);
                    var context = {
                        'data' : response,
                    };
                    var html = templateScript(context);
                    $('.information-patient').html(html);
                    $('#myModal').modal('hide');
                }
            });
        })

        $('#form_submit').on('submit', function(e) {
            e.preventDefault();
            let inputs = $(this).find('input');
            if (inputs.length > 3) {
                toastr.error('Không được chọn quá 3 xung');
                return;
            }
            inputs.each(function() {
                if ($(this).val() == '') {
                    let error = $(this).parent().find('.error');
                    error.css({'display' : 'block'});
                    error.text('Không được bỏ trống');
                    return;
                } else {
                    let error = $(this).parent().find('.error');
                    error.css({'display' : 'none'});
                    error.text('');
                }
            })
            var form = new FormData(this);
            $.ajax({
                url: '?mod=home&act=post',
                method: 'post',
                dataType:'JSON',
                processData: false,
                contentType: false,
                data:form,
                success:function(response){
                    var template = $('#handlebars-result').html();
                    var templateScript = Handlebars.compile(template);
                    var context = {
                        'symptom' : response.symptom,
                        'weight' : response.weight,
                    };
                    var html = templateScript(context);
                    $('.content-result').html(html);
                }
            });
        })
    })
</script>
</body>
</html>